import 'package:mfood/pages/cart/cart_page.dart';
import 'package:mfood/pages/food/popular_food_detail.dart';
import 'package:mfood/pages/food/recommend_food_detail.dart';
import 'package:mfood/pages/home/home_page.dart';
import 'package:mfood/pages/home/main_food_page.dart';
import 'package:mfood/pages/splash/splash_page.dart';
import 'package:get/get.dart';

import '../pages/auth/sign_in_page.dart';

class RouteHelper{
  static const String splashPage = "/splash-page";
  static const String initial = "/";
  static const String popularFood = "/popular-food";
  static const String recommendedFood = "/recommended-food";
  static const String cartPage = "/cart-page";
  static const String loginPage = "/login-page";


  static String getSplashPage() => '$splashPage';
  static String getInitial() => '$initial' ;
  static String getPopularFood(int pageId, String page) => '$popularFood?pageId=$pageId&page=$page';
  static String getRecommendedFood(int pageId, String page) => '$recommendedFood?pageId=$pageId&page=$page';
  static String getCartPage() => '$cartPage';
  static String getLoginPage() => '$loginPage';


  static List<GetPage> routes = [
    GetPage(name: splashPage, page: () => SplashScreen()),
    GetPage(name: initial, page: () => const HomePage()),
    GetPage(name: loginPage, page: () => SignInPage()),

    GetPage(name: popularFood, page: () {
      var pageId = Get.parameters['pageId'];
      var page = Get.parameters['page'];
      return PopularFoodDetail(pageId: int.parse(pageId!), page: page!);
    },
    transition: Transition.fadeIn
    ),

    GetPage(name: recommendedFood, page: () {
      var pageId = Get.parameters['pageId'];
      return RecommendFoodDetail(pageId: int.parse(pageId!));
    },
        transition: Transition.fadeIn
    ),
    GetPage(name: cartPage, page: (){
      return CartPage();
    },
      transition: Transition.fadeIn
    )
  ];
}