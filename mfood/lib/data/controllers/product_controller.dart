import 'package:get/get.dart';
import 'package:mfood/data/repository/product_repo.dart';

import '../../models/products_2_model.dart';

class ProductController extends GetxController{
  final ProductRepo productRepo;
  ProductController({required this.productRepo});
  List<dynamic> _productList = [];
  List<dynamic> get productList => _productList;

  Future<void> getProductList() async {
    Response response = await productRepo.getProductList();
    if(response.statusCode == 200) {
      _productList = [];
      _productList.addAll(Products.fromJson(response.body).products);
      update();
      print("productList:  " + _productList.toString());
      print("product status code: " + response.statusCode.toString());
    } else {
      print("product status code: " + response.statusCode.toString());
    }
  }
}
