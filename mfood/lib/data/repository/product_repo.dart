import 'package:get/get.dart';
import 'package:mfood/data/api/api_client.dart';
import 'package:mfood/utils/app_constraints.dart';

class ProductRepo extends GetxService{
  final ApiClient apiClient;
  ProductRepo({required this.apiClient});
  
  Future<Response> getProductList() async {
    return await apiClient.getData(AppConstrants.PRODUCT_URI);
  }
  
}
