import 'package:mfood/data/api/api_client.dart';
import 'package:mfood/utils/app_constraints.dart';
import 'package:get/get.dart';

class PopularProductRepo extends GetxService{
  final ApiClient apiClient;
  PopularProductRepo({required this.apiClient});

  Future<Response> getPopularProductList() async{
    return await apiClient.getData(AppConstrants.POPULAR_PRODUCT_URI);
  }
}