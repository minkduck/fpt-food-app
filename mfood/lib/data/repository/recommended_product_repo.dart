import 'package:mfood/data/api/api_client.dart';
import 'package:mfood/utils/app_constraints.dart';
import 'package:get/get.dart';

class RecommendedProductRepo extends GetxService{
  final ApiClient apiClient;
  RecommendedProductRepo({required this.apiClient});

  Future<Response> getRecommendedProductList() async{
    return await apiClient.getData(AppConstrants.RECOMMEND_PRODUCT_URI);
  }
}