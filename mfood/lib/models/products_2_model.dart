class Products {
  late List<ProductModel> _products;
  List<ProductModel> get products => _products;

  Products({required products}) {
    _products = products;
  }

  Products.fromJson(Map<String, dynamic> json) {
    if (json['products'] != null) {
      _products = <ProductModel>[];
      json['products'].forEach((v) {
        _products.add(ProductModel.fromJson(v));
      });
    }
  }
}

class ProductModel {
  int? id;
  int? categoryId;
  int? storeId;
  String? name;
  double? price;
  int? quantity;
  bool? status;
  String? picture;

  ProductModel(
    {this.id,
    this.categoryId,
    this.storeId,
    this.name,
    this.price,
    this.quantity,
    this.status,
    this.picture});

  ProductModel.fromJson(Map<String, dynamic> json) {
  id = json['id'];
  categoryId = json['categoryId'];
  storeId = json['storeId'];
  name = json['name'];
  price = json['price'];
  quantity = json['quantity'];
  status = json['status'];
  }

  Map<String, dynamic> toJson() {
    return{
      "id": this.id,
     "categoryId": this.categoryId,
     "storeId": this.storeId,
     "name": this.name,
     "price": this.price,
     "quantity": this.quantity,
     "status": this.status,
     "picture": this.picture,
    };
  }
}